﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component2
{
    public class Class2 : AbstractComponent, IGrzanie
    {
        public Class2()
        {
            this.RegisterProvidedInterface<IGrzanie>(this);
        }

        public string Wlacz()
        {
            return string.Format("Wlacz z Class2");
        }

        public string Wylacz()
        {
            return string.Format("Wylacz z Class2");
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }
    }
}
