﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;

namespace Lab4.Contract
{
    public interface IGrzanie
    {
        string Wlacz();
        string Wylacz();
    }
}
