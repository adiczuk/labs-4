﻿using Lab4.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;

namespace Lab4.Component1
{
    public class Class1 : AbstractComponent
    {
        public IGrzanie Grzalka;

        public Class1()
        {
            this.RegisterRequiredInterface<IGrzanie>();
        }

        public Class1(IGrzanie g)
        {
            this.Grzalka = g;
            this.RegisterProvidedInterface<IGrzanie>(this.Grzalka);
        }

        public override void InjectInterface(Type type, object impl)
        {
            
        }
    }
}
