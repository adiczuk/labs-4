﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Component1;
using Lab4.Component2;
using Lab4.Contract;

namespace Lab4.Main
{
    public class Program
    {
        static void Main(string[] args)
        {
            Container kontener = new Container();

            Class1 c1 = new Class1();
            Class2 c2 = new Class2();

            c2.RegisterProvidedInterface<IGrzanie>(c2);
            c1.RegisterRequiredInterface<IGrzanie>();

            kontener.RegisterComponents(c1, c2);
        }
    }
}
