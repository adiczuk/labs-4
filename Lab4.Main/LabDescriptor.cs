﻿using System;
using Lab4.Component1;
using Lab4.Component2;
using Lab4.Component2B;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Main
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component2);
        public delegate void RegisterComponentInContainer(object container, object component);
        public delegate bool AreDependenciesResolved(object container);

        #endregion

        #region P1

        public static Type Component1 = typeof(Class1);
        public static Type Component2 = typeof(Class2);

        public static Type RequiredInterface = typeof(IGrzanie);

        public static GetInstance GetInstanceOfRequiredInterface = (RequiredInterface) => RequiredInterface;
        
        #endregion

        #region P2

        public static Type Container = typeof(Container);

        public static RegisterComponentInContainer RegisterComponent = (container, component) => { (container as Container).RegisterComponent((IComponent)component); };

        public static AreDependenciesResolved ResolvedDependencied = (container) => { return ((Container)container).DependenciesResolved; };

        #endregion

        #region P3

        public static Type Component2B = typeof(Class3);

        #endregion
    }
}
