﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Component1;
using Lab4.Component2;
using Lab4.Component2B;
using Lab4.Contract;

namespace Lab4.MainB
{
    class Program
    {
        static void Main(string[] args)
        {
            Container kontener = new Container();

            Class1 c1 = new Class1();
            Class3 c3 = new Class3();

            c3.RegisterProvidedInterface<IGrzanie>(c3);
            c1.RegisterRequiredInterface<IGrzanie>();

            kontener.RegisterComponents(c1, c3);
        }
    }
}
