﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2B
{
    public class Class3 : AbstractComponent, IGrzanie
    {
        public Class3()
        {
            this.RegisterProvidedInterface<IGrzanie>(this);
        }

        public string Wlacz()
        {
            return string.Format("Wlacz z Class3");
        }

        public string Wylacz()
        {
            return string.Format("Wylacz z Class3");
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }
    }
}
